import smtplib

gmail_user = ''
gmail_password = ''
to = ['']


def send_email(subject='OMG Super Important Message', body='Hey, what'):
    email_text = """\
    From: %s
    To: %s
    Subject: %s

    %s
    """ % (gmail_user, ", ".join(to), subject, body)
    try:
        server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        server.ehlo()
        server.login(gmail_user, gmail_password)
        server.sendmail(gmail_user, to, email_text)
        server.close()
        print('Email sent')
    except:
        print('Something went wrong...')


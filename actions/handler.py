from services import mailer
import datetime


external_data = {
    "c5cbd98eaedea2457d1bb6ce": {
        "holiday_per_year": "30"
    }
}


def action_handler(detected_intent):
    action = detected_intent['detectedAction']
    print(action)
    if action == 'illness':
        return illness_action(detected_intent)
    elif action == 'basicAction':
        return example_action(detected_intent)
    elif action == 'replacement':
        return example_replacement(detected_intent)
    elif action == 'holidayPerYear':
        return get_holiday_per_year_for_user(detected_intent)
    else:
        return detected_intent


def get_holiday_per_year_for_user(detected_intent):
    user_id = detected_intent['recipient']
    holiday_per_year = external_data[user_id]['holiday_per_year']
    outputs = detected_intent['outputs']
    text = outputs[0]['message']['text']
    outputs[0]['message']['text'] = text.replace('{holiday_per_year}', holiday_per_year)
    print(outputs)
    return detected_intent


def example_action(detected_intent):
    print('Here you can do custom stuff')
    return detected_intent


def example_replacement(detected_intent):
    try:
        day_as_string = datetime.datetime.today().strftime("%A")
        outputs = detected_intent['outputs']
        text = outputs[0]['message']['text']
        outputs[0]['message']['text'] = text.replace('{current_day}', day_as_string)
    finally:
        return detected_intent


def illness_action(detected_intent):
    mailer.send_email('Notification of illness')
    return detected_intent


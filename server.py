from flask import Flask, request
from actions import handler

app = Flask(__name__)


@app.route("/")
def main():
    return 'Server is running'


@app.route("/botfriends-x", methods=['POST'])
def action_routing():
    detected_intent = request.json['detectedIntent']
    response = handler.action_handler(detected_intent)
    return response


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=4000)
